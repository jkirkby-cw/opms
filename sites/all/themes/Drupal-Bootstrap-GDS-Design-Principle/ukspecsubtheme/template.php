<?php

/**
 * @file
 * template.php
 */

function ukspecsubtheme_form_comment_form_alter(&$form, &$form_state, $form_id)
{
$form['comment_body']['#after_build'][] = 'remove_tips';
  }

  function remove_tips(&$form)
  {
      unset($form['und'][0]['format']['guidelines']);
      unset($form['und'][0]['format']['help']);
      unset($form['und'][0]['format']);
      return $form;
  }