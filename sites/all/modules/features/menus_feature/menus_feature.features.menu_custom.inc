<?php
/**
 * @file
 * menus_feature.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function menus_feature_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: devel.
  $menus['devel'] = array(
    'menu_name' => 'devel',
    'title' => 'Development',
    'description' => 'Development link',
  );
  // Exported menu: features.
  $menus['features'] = array(
    'menu_name' => 'features',
    'title' => 'Features',
    'description' => 'Menu items for any enabled features.',
  );
  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: management.
  $menus['management'] = array(
    'menu_name' => 'management',
    'title' => 'Management',
    'description' => 'The <em>Management</em> menu contains links for administrative tasks.',
  );
  // Exported menu: menu-account-menu.
  $menus['menu-account-menu'] = array(
    'menu_name' => 'menu-account-menu',
    'title' => 'Account Menu',
    'description' => '',
  );
  // Exported menu: menu-contractor-menu.
  $menus['menu-contractor-menu'] = array(
    'menu_name' => 'menu-contractor-menu',
    'title' => 'Contractor Menu',
    'description' => '',
  );
  // Exported menu: menu-employee-menu.
  $menus['menu-employee-menu'] = array(
    'menu_name' => 'menu-employee-menu',
    'title' => 'Employee Menu',
    'description' => '',
  );
  // Exported menu: menu-owner-add-content.
  $menus['menu-owner-add-content'] = array(
    'menu_name' => 'menu-owner-add-content',
    'title' => 'Add Documents',
    'description' => '',
  );
  // Exported menu: menu-owner-menu.
  $menus['menu-owner-menu'] = array(
    'menu_name' => 'menu-owner-menu',
    'title' => 'Owner Menu',
    'description' => '',
  );
  // Exported menu: menu-owner-properties.
  $menus['menu-owner-properties'] = array(
    'menu_name' => 'menu-owner-properties',
    'title' => 'Properties Add Content',
    'description' => '',
  );
  // Exported menu: menu-owners-calenders-menus.
  $menus['menu-owners-calenders-menus'] = array(
    'menu_name' => 'menu-owners-calenders-menus',
    'title' => 'Calenders Menus',
    'description' => '',
  );
  // Exported menu: menu-spreadsheet-template-menu.
  $menus['menu-spreadsheet-template-menu'] = array(
    'menu_name' => 'menu-spreadsheet-template-menu',
    'title' => 'Spreadsheet Template Menu',
    'description' => '',
  );
  // Exported menu: menu-tenant-menu.
  $menus['menu-tenant-menu'] = array(
    'menu_name' => 'menu-tenant-menu',
    'title' => 'Tenant Menu',
    'description' => '',
  );
  // Exported menu: navigation.
  $menus['navigation'] = array(
    'menu_name' => 'navigation',
    'title' => 'Navigation',
    'description' => 'The <em>Navigation</em> menu contains links intended for site visitors. Links are added to the <em>Navigation</em> menu automatically by some modules.',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Account Menu');
  t('Add Documents');
  t('Calenders Menus');
  t('Contractor Menu');
  t('Development');
  t('Development link');
  t('Employee Menu');
  t('Features');
  t('Main menu');
  t('Management');
  t('Menu items for any enabled features.');
  t('Navigation');
  t('Owner Menu');
  t('Properties Add Content');
  t('Spreadsheet Template Menu');
  t('Tenant Menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('The <em>Management</em> menu contains links for administrative tasks.');
  t('The <em>Navigation</em> menu contains links intended for site visitors. Links are added to the <em>Navigation</em> menu automatically by some modules.');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('User menu');


  return $menus;
}
