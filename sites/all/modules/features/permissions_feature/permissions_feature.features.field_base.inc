<?php
/**
 * @file
 * permissions_feature.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function permissions_feature_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_bank_account_number'
  $field_bases['field_bank_account_number'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_bank_account_number',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'encset',
    'settings' => array(
      'encryption' => 'mcrypt',
      'profile2_private' => 1,
      'usage' => 'free',
    ),
    'translatable' => 0,
    'type' => 'encset',
  );

  // Exported field_base: 'field_bank_name'
  $field_bases['field_bank_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_bank_name',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'encset',
    'settings' => array(
      'encryption' => 'mcrypt',
      'profile2_private' => 1,
      'usage' => 'free',
    ),
    'translatable' => 0,
    'type' => 'encset',
  );

  // Exported field_base: 'field_bank_sort_code'
  $field_bases['field_bank_sort_code'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_bank_sort_code',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'encset',
    'settings' => array(
      'encryption' => 'mcrypt',
      'profile2_private' => 1,
      'usage' => 'free',
    ),
    'translatable' => 0,
    'type' => 'encset',
  );

  // Exported field_base: 'field_contact_address'
  $field_bases['field_contact_address'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_contact_address',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'addressfield',
    'settings' => array(
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'addressfield',
  );

  // Exported field_base: 'field_contact_email'
  $field_bases['field_contact_email'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_contact_email',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'email',
    'settings' => array(
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'email',
  );

  // Exported field_base: 'field_contact_hours'
  $field_bases['field_contact_hours'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_contact_hours',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'office_hours',
    'settings' => array(
      'cardinality' => 2,
      'granularity' => 30,
      'hoursformat' => 1,
      'limitend' => '',
      'limitstart' => '',
      'profile2_private' => 0,
      'valhrs' => 1,
    ),
    'translatable' => 0,
    'type' => 'office_hours',
  );

  // Exported field_base: 'field_contact_number'
  $field_bases['field_contact_number'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_contact_number',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'telephone',
    'settings' => array(
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'telephone',
  );

  // Exported field_base: 'field_date_of_birth'
  $field_bases['field_date_of_birth'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_date_of_birth',
    'foreign keys' => array(),
    'indexes' => array(
      'date' => array(
        0 => 'day',
        1 => 'month',
        2 => 'year',
      ),
      'triggers' => array(
        0 => 'triggers',
      ),
    ),
    'locked' => 0,
    'module' => 'birthdays',
    'settings' => array(
      'profile2_private' => 1,
    ),
    'translatable' => 0,
    'type' => 'birthdays_date',
  );

  // Exported field_base: 'field_full_name'
  $field_bases['field_full_name'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_full_name',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
      'profile2_private' => 0,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_is_in_property'
  $field_bases['field_is_in_property'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_is_in_property',
    'field_permissions' => array(
      'type' => 2,
    ),
    'foreign keys' => array(
      'nid' => array(
        'columns' => array(
          'nid' => 'nid',
        ),
        'table' => 'node',
      ),
    ),
    'indexes' => array(
      'nid' => array(
        0 => 'nid',
      ),
    ),
    'locked' => 0,
    'module' => 'node_reference',
    'settings' => array(
      'profile2_private' => TRUE,
      'referenceable_types' => array(
        'announcements' => 0,
        'appliance_manuals' => 0,
        'casetracker_basic_case' => 0,
        'casetracker_basic_project' => 0,
        'event_calendar' => 0,
        'expenses' => 0,
        'insurance_documents' => 0,
        'itinerary' => 0,
        'maintenance_schedule' => 0,
        'page' => 0,
        'property' => 'property',
        'support_ticket' => 0,
        'tenancy_agreement' => 0,
      ),
      'view' => array(
        'args' => array(),
        'display_name' => '',
        'view_name' => '',
      ),
    ),
    'translatable' => 0,
    'type' => 'node_reference',
  );

  // Exported field_base: 'field_national_insurance_number'
  $field_bases['field_national_insurance_number'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_national_insurance_number',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'encset',
    'settings' => array(
      'encryption' => 'mcrypt',
      'profile2_private' => 1,
      'usage' => 'free',
    ),
    'translatable' => 0,
    'type' => 'encset',
  );

  return $field_bases;
}
