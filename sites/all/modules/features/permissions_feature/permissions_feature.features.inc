<?php
/**
 * @file
 * permissions_feature.features.inc
 */

/**
 * Implements hook_default_og_membership_type().
 */
function permissions_feature_default_og_membership_type() {
  $items = array();
  $items['contractor'] = entity_import('og_membership_type', '{
    "name" : "contractor",
    "description" : "Contractor",
    "language" : "",
    "rdf_mapping" : []
  }');
  $items['employee'] = entity_import('og_membership_type', '{
    "name" : "employee",
    "description" : "Employee",
    "language" : "",
    "rdf_mapping" : []
  }');
  $items['tenant'] = entity_import('og_membership_type', '{
    "name" : "tenant",
    "description" : "Tenant",
    "language" : "",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_profile2_type().
 */
function permissions_feature_default_profile2_type() {
  $items = array();
  $items['contractor'] = entity_import('profile2_type', '{
    "userCategory" : false,
    "userView" : false,
    "type" : "contractor",
    "label" : "Contractor",
    "weight" : "0",
    "data" : { "registration" : 0, "use_page" : 1 },
    "rdf_mapping" : []
  }');
  $items['employee'] = entity_import('profile2_type', '{
    "userCategory" : false,
    "userView" : false,
    "type" : "employee",
    "label" : "Employee",
    "weight" : "0",
    "data" : { "registration" : 0, "use_page" : 1 },
    "rdf_mapping" : []
  }');
  $items['tenant'] = entity_import('profile2_type', '{
    "userCategory" : false,
    "userView" : false,
    "type" : "tenant",
    "label" : "Tenant",
    "weight" : "0",
    "data" : { "registration" : 0, "use_page" : 1 },
    "rdf_mapping" : []
  }');
  return $items;
}
