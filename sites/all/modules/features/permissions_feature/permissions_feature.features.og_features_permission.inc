<?php
/**
 * @file
 * permissions_feature.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function permissions_feature_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:property:add user'
  $permissions['node:property:add user'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:administer group'
  $permissions['node:property:administer group'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:approve and deny subscription'
  $permissions['node:property:approve and deny subscription'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:create announcements content'
  $permissions['node:property:create announcements content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:create appliance_manuals content'
  $permissions['node:property:create appliance_manuals content'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:create expenses content'
  $permissions['node:property:create expenses content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:create insurance_documents content'
  $permissions['node:property:create insurance_documents content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:create itinerary content'
  $permissions['node:property:create itinerary content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:create maintenance_schedule content'
  $permissions['node:property:create maintenance_schedule content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:create other_documents content'
  $permissions['node:property:create other_documents content'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:create tenancy_agreement content'
  $permissions['node:property:create tenancy_agreement content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:create ticket content'
  $permissions['node:property:create ticket content'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:delete any announcements content'
  $permissions['node:property:delete any announcements content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:delete any appliance_manuals content'
  $permissions['node:property:delete any appliance_manuals content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete any expenses content'
  $permissions['node:property:delete any expenses content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete any insurance_documents content'
  $permissions['node:property:delete any insurance_documents content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete any itinerary content'
  $permissions['node:property:delete any itinerary content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete any maintenance_schedule content'
  $permissions['node:property:delete any maintenance_schedule content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:delete any other_documents content'
  $permissions['node:property:delete any other_documents content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete any tenancy_agreement content'
  $permissions['node:property:delete any tenancy_agreement content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete any ticket content'
  $permissions['node:property:delete any ticket content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete own announcements content'
  $permissions['node:property:delete own announcements content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:delete own appliance_manuals content'
  $permissions['node:property:delete own appliance_manuals content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete own expenses content'
  $permissions['node:property:delete own expenses content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete own insurance_documents content'
  $permissions['node:property:delete own insurance_documents content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete own itinerary content'
  $permissions['node:property:delete own itinerary content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete own maintenance_schedule content'
  $permissions['node:property:delete own maintenance_schedule content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:delete own other_documents content'
  $permissions['node:property:delete own other_documents content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete own tenancy_agreement content'
  $permissions['node:property:delete own tenancy_agreement content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:delete own ticket content'
  $permissions['node:property:delete own ticket content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:manage members'
  $permissions['node:property:manage members'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:manage permissions'
  $permissions['node:property:manage permissions'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:manage roles'
  $permissions['node:property:manage roles'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:subscribe'
  $permissions['node:property:subscribe'] = array(
    'roles' => array(
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:subscribe without approval'
  $permissions['node:property:subscribe without approval'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:unsubscribe'
  $permissions['node:property:unsubscribe'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:update any announcements content'
  $permissions['node:property:update any announcements content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:update any appliance_manuals content'
  $permissions['node:property:update any appliance_manuals content'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update any expenses content'
  $permissions['node:property:update any expenses content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update any insurance_documents content'
  $permissions['node:property:update any insurance_documents content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update any itinerary content'
  $permissions['node:property:update any itinerary content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update any maintenance_schedule content'
  $permissions['node:property:update any maintenance_schedule content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:update any other_documents content'
  $permissions['node:property:update any other_documents content'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update any tenancy_agreement content'
  $permissions['node:property:update any tenancy_agreement content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update any ticket content'
  $permissions['node:property:update any ticket content'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update body field'
  $permissions['node:property:update body field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:update event_calendar_date field'
  $permissions['node:property:update event_calendar_date field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_body_owner_employee_contra field'
  $permissions['node:property:update field_body_owner_employee_contra field'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:update field_cost field'
  $permissions['node:property:update field_cost field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_deposit_amount field'
  $permissions['node:property:update field_deposit_amount field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_expense_date field'
  $permissions['node:property:update field_expense_date field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_expense_type field'
  $permissions['node:property:update field_expense_type field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_image field'
  $permissions['node:property:update field_image field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_images field'
  $permissions['node:property:update field_images field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:update field_is_in_property field'
  $permissions['node:property:update field_is_in_property field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_issue field'
  $permissions['node:property:update field_issue field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:update field_itinerary_sheet field'
  $permissions['node:property:update field_itinerary_sheet field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_landlord_signature field'
  $permissions['node:property:update field_landlord_signature field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_minimum_term field'
  $permissions['node:property:update field_minimum_term field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_number_of_bathrooms field'
  $permissions['node:property:update field_number_of_bathrooms field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_number_of_bedrooms field'
  $permissions['node:property:update field_number_of_bedrooms field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_pdf_document field'
  $permissions['node:property:update field_pdf_document field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_policy_start field'
  $permissions['node:property:update field_policy_start field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_price_per_month field'
  $permissions['node:property:update field_price_per_month field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_rent_amount field'
  $permissions['node:property:update field_rent_amount field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_rent_due_date field'
  $permissions['node:property:update field_rent_due_date field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_responsibility field'
  $permissions['node:property:update field_responsibility field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:update field_tags field'
  $permissions['node:property:update field_tags field'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:update field_tenant_signature field'
  $permissions['node:property:update field_tenant_signature field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_the_landlord field'
  $permissions['node:property:update field_the_landlord field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_the_tenant field'
  $permissions['node:property:update field_the_tenant field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_ticket_department field'
  $permissions['node:property:update field_ticket_department field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:update field_ticket_status field'
  $permissions['node:property:update field_ticket_status field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update field_work_day field'
  $permissions['node:property:update field_work_day field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:update group'
  $permissions['node:property:update group'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update group_group field'
  $permissions['node:property:update group_group field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:update og_group_ref field'
  $permissions['node:property:update og_group_ref field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:update og_user_node field'
  $permissions['node:property:update og_user_node field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:update own announcements content'
  $permissions['node:property:update own announcements content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:update own appliance_manuals content'
  $permissions['node:property:update own appliance_manuals content'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update own expenses content'
  $permissions['node:property:update own expenses content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update own insurance_documents content'
  $permissions['node:property:update own insurance_documents content'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update own itinerary content'
  $permissions['node:property:update own itinerary content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update own maintenance_schedule content'
  $permissions['node:property:update own maintenance_schedule content'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:update own other_documents content'
  $permissions['node:property:update own other_documents content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update own tenancy_agreement content'
  $permissions['node:property:update own tenancy_agreement content'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:update own ticket content'
  $permissions['node:property:update own ticket content'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:view body field'
  $permissions['node:property:view body field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view event_calendar_date field'
  $permissions['node:property:view event_calendar_date field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_body_owner_employee_contra field'
  $permissions['node:property:view field_body_owner_employee_contra field'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:view field_cost field'
  $permissions['node:property:view field_cost field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:view field_deposit_amount field'
  $permissions['node:property:view field_deposit_amount field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_expense_date field'
  $permissions['node:property:view field_expense_date field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:view field_expense_type field'
  $permissions['node:property:view field_expense_type field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:view field_image field'
  $permissions['node:property:view field_image field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:view field_images field'
  $permissions['node:property:view field_images field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:view field_is_in_property field'
  $permissions['node:property:view field_is_in_property field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:view field_issue field'
  $permissions['node:property:view field_issue field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:view field_itinerary_sheet field'
  $permissions['node:property:view field_itinerary_sheet field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_landlord_signature field'
  $permissions['node:property:view field_landlord_signature field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_minimum_term field'
  $permissions['node:property:view field_minimum_term field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_number_of_bathrooms field'
  $permissions['node:property:view field_number_of_bathrooms field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:view field_number_of_bedrooms field'
  $permissions['node:property:view field_number_of_bedrooms field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:view field_pdf_document field'
  $permissions['node:property:view field_pdf_document field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_policy_start field'
  $permissions['node:property:view field_policy_start field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:view field_price_per_month field'
  $permissions['node:property:view field_price_per_month field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:view field_rent_amount field'
  $permissions['node:property:view field_rent_amount field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_rent_due_date field'
  $permissions['node:property:view field_rent_due_date field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_responsibility field'
  $permissions['node:property:view field_responsibility field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:property:view field_tags field'
  $permissions['node:property:view field_tags field'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:property:view field_tenant_signature field'
  $permissions['node:property:view field_tenant_signature field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_the_landlord field'
  $permissions['node:property:view field_the_landlord field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_the_tenant field'
  $permissions['node:property:view field_the_tenant field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_ticket_department field'
  $permissions['node:property:view field_ticket_department field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_ticket_status field'
  $permissions['node:property:view field_ticket_status field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view field_work_day field'
  $permissions['node:property:view field_work_day field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view group_group field'
  $permissions['node:property:view group_group field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:property:view og_group_ref field'
  $permissions['node:property:view og_group_ref field'] = array(
    'roles' => array(
      'Contractor' => 'Contractor',
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:property:view og_user_node field'
  $permissions['node:property:view og_user_node field'] = array(
    'roles' => array(
      'Employee' => 'Employee',
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  return $permissions;
}
