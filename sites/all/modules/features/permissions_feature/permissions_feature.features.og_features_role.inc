<?php
/**
 * @file
 * permissions_feature.features.og_features_role.inc
 */

/**
 * Implements hook_og_features_default_roles().
 */
function permissions_feature_og_features_default_roles() {
  $roles = array();

  // Exported OG Role: 'node:property:Contractor'.
  $roles['node:property:Contractor'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'property',
    'name' => 'Contractor',
  );

  // Exported OG Role: 'node:property:Employee'.
  $roles['node:property:Employee'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'property',
    'name' => 'Employee',
  );

  return $roles;
}
