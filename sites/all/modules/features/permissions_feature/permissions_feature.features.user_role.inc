<?php
/**
 * @file
 * permissions_feature.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function permissions_feature_user_default_roles() {
  $roles = array();

  // Exported role: Contract(Requires Action).
  $roles['Contract(Requires Action)'] = array(
    'name' => 'Contract(Requires Action)',
    'weight' => 7,
  );

  // Exported role: Contractor.
  $roles['Contractor'] = array(
    'name' => 'Contractor',
    'weight' => 4,
  );

  // Exported role: Employee.
  $roles['Employee'] = array(
    'name' => 'Employee',
    'weight' => 6,
  );

  // Exported role: Owner.
  $roles['Owner'] = array(
    'name' => 'Owner',
    'weight' => 5,
  );

  // Exported role: Tenant.
  $roles['Tenant'] = array(
    'name' => 'Tenant',
    'weight' => 3,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  return $roles;
}
